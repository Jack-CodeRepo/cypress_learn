
export function aller_sur_url(url){
    cy.visit(url)
}


export function cliquer_sur_element(selector){
    cy.get(selector, {timeout: 10_000}).should('be.visible').click()
}