import '../functions/homepage_methods.js'

import urls from '../fixtures/urls.json'

import { 
    aller_sur_url, 
    cliquer_sur_element 
} from '../functions/homepage_methods.js'




describe('empty spec', () => {

  Cypress.Cookies.debug(true)

  it('Aller sur la main url', () => {
    aller_sur_url(urls["main"])      // urls.main fonctionne egalement
  })


  it('Valider les rgpd', () => {
    cliquer_sur_element('[id="onetrust-close-btn-container"]')
  })


  // ici l'usage de cy.fixture limite le scope d'accessibilité de url.json
  it('Aller sur Assurance auto', () => {
    cy.fixture('urls.json').then(url_data =>{
        aller_sur_url(url_data.assurance_auto)
    })
  })
})

